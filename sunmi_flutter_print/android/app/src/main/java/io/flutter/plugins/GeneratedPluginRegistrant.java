package io.flutter.plugins;

import com.king.sunmi_flutter_print.SunmiFlutterPrintPlugin;

import io.flutter.plugin.common.PluginRegistry;

/**
 * Generated file. Do not edit.
 */
public final class GeneratedPluginRegistrant {
    public static void registerWith(PluginRegistry registry) {
        if (alreadyRegisteredWith(registry)) {
            return;
        }
        SunmiFlutterPrintPlugin.registerWith(registry.registrarFor("com.king.sunmi_flutter_print.SunmiFlutterPrintPlugin"));
    }

    private static boolean alreadyRegisteredWith(PluginRegistry registry) {
        final String key = GeneratedPluginRegistrant.class.getCanonicalName();
        if (registry.hasPlugin(key)) {
            return true;
        }
        registry.registrarFor(key);
        return false;
    }
}
