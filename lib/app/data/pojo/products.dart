class Product {
  int id;
  String productName;
  double price;
  String imageProduct;

  Product({
    this.id,
    this.productName,
    this.price,
    this.imageProduct,
  });
}
