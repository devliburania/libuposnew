import 'package:flutter/material.dart';
import 'package:libupos/app/pages/point_of_sale.dart';
import 'package:libupos/app/tabs/tab_pos.dart';
import 'package:libupos/app/utility/drawer.dart';

import '../pages/point_of_sale.dart';
import '../pages/profile.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 2,
      child: Scaffold(
          drawer: new MyDrawer(),
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(0, 193, 182, 1),
            bottom: TabBar(
              tabs: [
                Tab(
                  child: Text('POS'),
                ),
                Tab(
                  child: Text('Simple'),
                ),
              ],
            ),
            title: Text('Libu POS'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.account_box),
                onPressed: () {
                  //push(ProfilePage());
                },
              ),
            ],
          ),
          body: new TabBarView(
            children: <Widget>[
              new TabPOS(),
              new PosPage(),
            ],
          )),
    );
  }
}
