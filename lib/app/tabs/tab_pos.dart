import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:libupos/app/data/pojo/products.dart';
import 'package:libupos/app/data/services/odoo_response.dart';
import 'package:libupos/app/pages/shopingcart.dart';
import 'package:libupos/app/utility/strings.dart';
import '../../base.dart';
import '../data/pojo/products.dart';
import '../utility/constant.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class TabPOS extends StatefulWidget {
  @override
  _TabPOSState createState() => _TabPOSState();
}

class _TabPOSState extends Base<TabPOS>
    with AutomaticKeepAliveClientMixin<TabPOS> {
  final TextEditingController _controller = new TextEditingController();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final formatCurrency = new NumberFormat.compact();

  List<Product> _product = List<Product>();

  int _countitem = 0;
  double _pricecount = 0;

  var f = new NumberFormat("#,###", "en_US");
  List<Map<String, dynamic>> sendProduct = [];

  _changeCountItemUi(i) {
    var itemcount = _countitem;
    var pricecount = _pricecount;
    var _sendProduct = sendProduct;
    var searchproduct = _sendProduct
        .where((cart) => cart['id'].toString() == i.id.toString())
        .toList();

    if (searchproduct.length == 0) {
      _sendProduct
          .add({'id': i.id, 'name': i.productName, 'price': i.price, 'qty': 1});
      pricecount = pricecount + i.price;
    } else {
      var qty = searchproduct[0]['qty'];
      searchproduct[0]['qty'] = qty + 1;
      pricecount = NumberFormat.simpleCurrency(name: "", decimalDigits: 2)
          .format(int.parse(pricecount.toString())) as double;
    }
    itemcount = itemcount + 1;

    setState(() {
      _countitem = itemcount;
      _pricecount = pricecount;
      sendProduct = _sendProduct;
    });
  }

  _getProduct() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        showLoading();
        odoo.searchRead(Strings.product_template, [],
            ['price', 'name', 'id', 'image', 'list_price']).then(
              (OdooResponse res) {
            if (!res.hasError()) {
              setState(() {
                hideLoading();

                var productimage;
                for (var i in res.getRecords()) {
                  if (i["image"] != false) {
                    productimage = i["image"];
                  } else {
                    productimage = "";
                  }
                  _product.add(
                    new Product(
                      id: i["id"],
                      productName: i["name"] is! bool ? i["name"] : "N/A",
                      price: i["list_price"] is! bool ? i["list_price"] : "N/A",
                      imageProduct: productimage,
                    ),
                  );
                }
              });
            } else {
              print(res.getError());
              showMessage("Warning", res.getErrorMessage());
            }
          },
        );
      }
    });
  }

  _addProduct() {
    print('test');
  }

  _onSubmitSearch(String val) async {
    if (val != "") {
      _product.clear();

      setState(() {
        _product
            .where(
                (x) => x.productName.toLowerCase().contains(val.toLowerCase()))
            .toList();
      });
    } else {
      getOdooInstance().then((odoo) {
        _getProduct();
      });
    }
  }

  void _clearAllItems() {
    _countitem = 0;
    _pricecount = 0;
    sendProduct.clear();
    setState(() {
      _countitem = _countitem;
      _pricecount = _pricecount;
      sendProduct = sendProduct;
    });
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    initializeDateFormatting('id');
    getOdooInstance().then((odoo) {
      _getProduct();
    });
  }

  Widget rowProduct(Product product) {
    log(product.imageProduct.trimRight());

    String image = product.imageProduct.trimRight();
    var decodedBytes;

    Uint8List bytes;
    try {
      bytes = base64Decode(image.replaceAll("\n", ""));
    } catch (e) {
      print(e.toString());
    }
    return Card(
        color: Colors.grey[200],
        elevation: 0.0,
        margin: EdgeInsets.all(0.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0))),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          child: Center(
            child: Container(
              child: new Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10.0),
                    width: 80,
                    height: 80,
                    child: new Image.memory(bytes),
                  ),
                  Container(
                    padding: new EdgeInsets.only(
                        left: 10.0, top: 7.0, bottom: 3.0, right: 10),
                    child: Text("Rp. " + f.format(product.price),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.red,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding: new EdgeInsets.only(
                        left: 10.0, top: 3.0, bottom: 0.0, right: 10),
                    child: Text(product.productName.toString(),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.grey[500],
                            fontSize: 12.0,
                            fontWeight: FontWeight.w600)),
                  ),
                ],
              ),
            ),
          ),
          onTap: () {
            _changeCountItemUi(product);
            print(sendProduct);
          },
        ));
  }

  Widget buildProduct() {
    return new Container(
        width: double.infinity,
        child: GridView.builder(
            scrollDirection: Axis.vertical,
            physics: AlwaysScrollableScrollPhysics(),
            itemCount: _product.length,
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 2, mainAxisSpacing: 2, crossAxisCount: 2
              //childAspectRatio: 1.5,
            ),
            itemBuilder: (context, index) {
              return rowProduct(_product[index]);
            }));
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: 80,
            child: Form(
              key: _formKey,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: Container(
                      width: 300,
                      padding:
                      new EdgeInsets.only(top: 0, right: 10.0, left: 10.0),
                      child: Container(
                        width: 200,
                        padding: const EdgeInsets.only(left: 0, right: 0),
                        decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                        child: TextFormField(
                          decoration: InputDecoration(
                            labelStyle: GoogleFonts.lato(
                                fontStyle: FontStyle.normal,
                                color: Colors.grey[500],
                                fontSize: 15.0,
                                fontWeight: FontWeight.w700),
                            hintText: "Search",
                            hintStyle: GoogleFonts.lato(
                                fontStyle: FontStyle.normal,
                                color: Colors.grey[600],
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500),
                            fillColor: Colors.black,
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 0.0),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            contentPadding: EdgeInsets.all(10.0),
                            isDense: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          controller: _controller,
                          onFieldSubmitted: (String submittedStr) {
                            _onSubmitSearch(submittedStr);
                            _controller.text = submittedStr;
                          },
                        ),
                      ),
                    ),
                  ),
                  Container(
                      child: InkWell(
                        splashColor: Colors.red, // inkwell color
                        child: SizedBox(
                          width: 40,
                          height: 40,
                          child: Icon(
                            FontAwesomeIcons.qrcode,
                            color: Colors.grey,
                            size: 20,
                          ),
                        ),
                        onTap: () {
                          // _onSubmit(_controller.text);
                        },
                      ))
                ],
              ),
            ),
          ),
          Container(
              alignment: Alignment.topLeft,
              padding: new EdgeInsets.only(
                  top: MediaQuery
                      .of(context)
                      .size
                      .height * .10,
                  right: 10.0,
                  left: 10.0),
              child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(right: 10.0),
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(color: AppColors.color1)),
                          color: Colors.white,
                          textColor: Colors.red,
                          padding: EdgeInsets.all(8.0),
                          onPressed: () {
                            _clearAllItems();
                          },
                          child: Text(
                            "Kosongkan",
                            style: GoogleFonts.lato(
                                fontStyle: FontStyle.normal,
                                color: Colors.grey[500],
                                fontSize: 14.0,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () => push(ShopingCart(posData: sendProduct)),
                          child: Container(
                            height: 38,
                            decoration: BoxDecoration(
                              // color: DesignCourseAppTheme.nearlyBlue,
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(5.0),
                                ),
                                color: AppColors.color1),
                            child: Center(
                              child: Text(
                                //'$_countitem f.format($_pricecount)',
                                _countitem.toString() +
                                    "  Item  " +
                                    ' Rp. ' +
                                    f.format(_pricecount),
                                textAlign: TextAlign.left,
                                style: GoogleFonts.lato(
                                    fontStyle: FontStyle.normal,
                                    color: Colors.white,
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ))),
          Container(
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Container(
                    width: 300,
                    padding:
                    new EdgeInsets.only(top: 0, right: 10.0, left: 10.0),
                    child: Container(
                      width: 200,
                      padding: const EdgeInsets.only(left: 0, right: 0),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelStyle: GoogleFonts.lato(
                              fontStyle: FontStyle.normal,
                              color: Colors.grey[500],
                              fontSize: 15.0,
                              fontWeight: FontWeight.w700),
                          hintText: "Search",
                          hintStyle: GoogleFonts.lato(
                              fontStyle: FontStyle.normal,
                              color: Colors.grey[600],
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500),
                          fillColor: Colors.black,
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          contentPadding: EdgeInsets.all(10.0),
                          isDense: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        controller: _controller,
                        onFieldSubmitted: (String submittedStr) {
                          _onSubmitSearch(submittedStr);
                          _controller.text = submittedStr;
                        },
                      ),
                    ),
                  ),
                ),
                Container(
                    child: InkWell(
                      splashColor: Colors.red, // inkwell color
                      child: SizedBox(
                        width: 40,
                        height: 40,
                        child: Icon(
                          FontAwesomeIcons.qrcode,
                          color: Colors.grey,
                          size: 20,
                        ),
                      ),
                      onTap: () {
                        // _onSubmit(_controller.text);
                      },
                    ))
              ],
            ),
          ),
          Container(
              alignment: Alignment.topLeft,
              padding: new EdgeInsets.only(
                  top: MediaQuery
                      .of(context)
                      .size
                      .height * .19,
                  right: 3.0,
                  left: 3.0),
              child: Container(child: buildProduct()))
        ],
      ),
      // child: buildProduct(),
    );
  }
}
