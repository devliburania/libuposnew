import 'dart:ui';

class Constants {
  static const String UID = "uid";
  static const String SESSION_ID = "session_id";
  static const String USER_PREF = "UserPrefs";
  static const String ODOO_URL = "odooUrl";
  static const String SESSION = "session";
  static const String PERSON_ID = "person_id";
  static const String ACTIVE_SESSION = "db_activeSession";
  static const String POSCONFIG_ID = "db_posSession";
  static const String PRODUCT = "db_product";
}


class AppColors {
  static Color color1 = Color(0xfff00c1b5);
  static Color color2 = Color(0xfffffcc00);
  static Color color3 = Color(0xfffadf5f1);
}

