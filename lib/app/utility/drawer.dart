import 'package:flutter/material.dart';
import 'package:libupos/app/data/services/odoo_response.dart';
import 'package:libupos/app/pages/login.dart';
import 'package:libupos/app/utility/strings.dart';
import 'package:libupos/base.dart';

import 'constant.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends Base<MyDrawer> {
  String name = "";
  String image_URL = "";
  String email = "";
  var phone = "";
  var mobile = "";
  var street = "";
  var street2 = "";
  var city = "";
  var state_id = "";
  var zip = "";
  var title = "";
  var website = "";
  var jobposition = "";

  _getUserData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead(Strings.res_users, [
          ["id", "=", getUID()]
        ], []).then(
              (OdooResponse res) {
            if (!res.hasError()) {
              setState(() {
                String session = getSession();
                session = session.split(",")[0].split(";")[0];
                final result = res.getResult()['records'][0];
                name = result['name'] is! bool ? result['name'] : "";
                image_URL = getURL() +
                    "/web/image?model=res.users&field=image&" +
                    session +
                    "&id=" +
                    getUID().toString();
                email = result['email'] is! bool ? result['email'] : "";
              });
            } else {
              showMessage("Warning", res.getErrorMessage());
            }
          },
        );
      }
    });
  }

  _clearPrefs() async {
    odoo.destroy();

    preferences.remove(Constants.USER_PREF);
    preferences.remove(Constants.SESSION);
    preferences.remove(Constants.ODOO_URL);
    pushAndRemoveUntil(Login());
  }

  _getProfileData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("res.partner", [
          ["id", "=", user.result.partnerId] //model
        ], []).then(
              (OdooResponse res) {
            if (!res.hasError()) {
              setState(() {
                final result = res.getResult()['records'][0];
                phone = result['phone'] is! bool ? result['phone'] : "N/A";
                mobile = result['mobile'] is! bool ? result['mobile'] : "N/A";
                street = result['street'] is! bool ? result['street'] : "";
                street2 = result['street2'] is! bool ? result['street2'] : "";
                city = result['city'] is! bool ? result['city'] : "";
                state_id =
                result['state_id'] is! bool ? result['state_id'][1] : "";
                zip = result['zip'] is! bool ? result['zip'] : "";
                title = result['title'] is! bool ? result['title'][1] : "N/A";
                website =
                result['website'] is! bool ? result['website'] : "N/A";
                jobposition =
                result['function'] is! bool ? result['function'] : "N/A";
              });
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();

    getOdooInstance().then((odoo) {
      _getUserData();
      _getProfileData();
    });
  }


  @override
  Widget build(BuildContext context) {
    return new Drawer(
        child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/libuposbg.png"), fit: BoxFit.cover),
            ),
            child: ListView(
              children: <Widget>[
                ListTile(
                    leading:
                    CircleAvatar(backgroundImage: NetworkImage(image_URL)),
                    title: Text(
                      name,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        letterSpacing: 0.0,
                        color: Colors.white,
                      ),
                    ),
                    subtitle: Text(
                      email,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        letterSpacing: 0.0,
                        color: Colors.white,
                      ),
                    )),
                ListTile(
                    leading: Icon(Icons.home, color: Colors.white), title: Text(
                  "Home",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15,
                    letterSpacing: 0.0,
                    color: Colors.white,
                  ),
                )),
                ListTile(leading: Icon(Icons.grid_on, color: Colors.white,),
                    title: Text(
                      "Product",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        letterSpacing: 0.0,
                        color: Colors.white,
                      ),
                    )),
                ListTile(
                    leading: Icon(Icons.contacts, color: Colors.white),
                    title: Text(
                      "Contact Us",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        letterSpacing: 0.0,
                        color: Colors.white,
                      ),
                    )),
                ListTile(
                    leading: Icon(Icons.contacts, color: Colors.white),
                    title: Text(
                      "Log Out",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                        letterSpacing: 0.0,
                        color: Colors.white,
                      ),

                    ),
                    onTap: () {
                      _clearPrefs();
                    }),
              ],
            )));
  }
}
