import 'package:flutter/material.dart';
import 'package:libupos/app/utility/rounded_bordered_container.dart';
import 'package:libupos/base.dart';
import 'package:libupos/app/pages/checkout.dart';

class CartTwoPage extends StatefulWidget {
  final List posData;

  CartTwoPage({Key key, this.posData}) : super(key: key);

  @override
  _CartTwoPageState createState() => new _CartTwoPageState(posData);
}

class _CartTwoPageState extends State<CartTwoPage> {
  List posData;

  _CartTwoPageState(this.posData);

  double _subtotal = 0.0;
  double _ppn = 0.0;
  double _total = 0.0;

  _calculatePosData() {
    double pos_subtotal = 0.0;
    for (int i = 0; i < posData.length; i++) {
      double orderSubtotal = posData[i]['price'] * posData[i]['qty'];
      pos_subtotal += orderSubtotal;
    }

    setState(() {
      _subtotal = pos_subtotal;
      _total = pos_subtotal;
    });
  }

  _addQty(order) {
    var posorder = posData[order];
    posData[order]['qty'] = posorder['qty'] + 1;
    _calculatePosData();
  }

  _minQTy(order) {
    var posorder = posData[order];
    if (posData[order]['qty'] > 0) {
      posData[order]['qty'] = posorder['qty'] - 1;
      _calculatePosData();
    }
  }

  @override
  void initState() {
    super.initState();
    _calculatePosData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Cart'),
          backgroundColor: Color.fromRGBO(0, 193, 182, 1),
        ),
        body: Column(
          children: <Widget>[
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: posData.length > 0
                    ? ListView.builder(
                  itemCount: posData.length,
                  itemBuilder: (context, index) {
                    return cartItems(index);
                  },
                )
                    : null,
              ),
            ),
            platNumber(),
            _checkoutSection(context)
          ],
        ));
  }

  Widget platNumber() {
    return RoundedContainer(
      padding: const EdgeInsets.all(0),
      margin: EdgeInsets.all(10),
      height: 100,
      child: Row(
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          "Plat Nomor Kendaraan",
                          overflow: TextOverflow.fade,
                          softWrap: true,
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 20),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(width: 300, child: TextFormField()),
                      Spacer(),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget cartItems(int index) {
    return RoundedContainer(
      padding: const EdgeInsets.all(0),
      margin: EdgeInsets.all(10),
      height: 100,
      child: Row(
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          posData[index]['name'],
                          overflow: TextOverflow.fade,
                          softWrap: true,
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 18),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Rp. " + posData[index]['price'].toString() + ",-",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                      Spacer(),
                      Row(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              _minQTy(index);
                            },
                            splashColor: Colors.redAccent.shade200,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50)),
                              alignment: Alignment.center,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: Icon(
                                  Icons.remove,
                                  color: Colors.green,
                                  size: 30,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50)),
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                posData[index]['qty'].toString(),
                                style: TextStyle(
                                  fontSize: 25,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          InkWell(
                            onTap: () {
                              _addQty(index);
                            },
                            splashColor: Colors.lightBlue,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50)),
                              alignment: Alignment.center,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.green,
                                  size: 30,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _checkoutSection(context) {
    return Material(
      color: Color.fromRGBO(0, 193, 182, 0.2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(top: 30, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: [
                      Text(
                        "Sub Total :",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                      Spacer(),
                      Text(
                        "Rp. $_subtotal",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "PPN 10% :",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                      Spacer(),
                      Text(
                        "Rp. $_ppn",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "Total :",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                      Spacer(),
                      Text(
                        "Rp. $_total",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                    ],
                  )
                ],
              )),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Material(
              color: Color.fromRGBO(0, 193, 182, 1),
              borderRadius: BorderRadius.circular(10),
              elevation: 1.0,
              child: InkWell(
                splashColor: Color.fromRGBO(0, 193, 182, 1),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              Checkout(posData: posData)));
                },
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "BAYAR",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
