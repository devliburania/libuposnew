import 'package:flutter/material.dart';
import 'package:flutter_ticket_widget/flutter_ticket_widget.dart';


void main() =>
    runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    ));

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(0, 193, 182, 1),
      body: Container(
        padding: const EdgeInsets.only(top: 100.0),
        child: Column(
          children: <Widget>[
            Center(
              child: FlutterTicketWidget(
                width: 350.0,
                height: 500.0,
                isCornerRounded: true,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                              padding: const EdgeInsets.only(left: 250.0),
                              child: Image(
                                image: AssetImage('assets/images/tmii.jpg'),
                                width: 50.0,
                                height: 50.0,
                              )),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Text(
                          'Ticket',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: Column(
                          children: <Widget>[
                            ticketDetailsWidget(
                                'Tourist', 'Valentino', 'Date', '14-02-2020'),
                            Padding(
                              padding:
                              const EdgeInsets.only(top: 12.0, right: 10.0),
                              child: ticketDetailsWidget(
                                  'Sisa Voucher', '10', 'Kode', 'JXJAWYE'),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 40.0, left: 30.0, right: 30.0),
                          child: Center(
                            child: Container(
                              width: 100.0,
                              height: 100.0,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('assets/images/qr.png'),
                                      fit: BoxFit.cover)),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Center(
                            child: Text(
                              '9824 0972 1742 1298',
                              style: TextStyle(
                                color: Colors.black,
                              ),
                            ),
                          ))
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding:
              const EdgeInsets.only(top: 20.0, right: 70.0, left: 70.0),
              child: Row(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: 100.0,
                    height: 50.0,
                    buttonColor: Colors.yellow,
                    child: RaisedButton(
                        child: Text(
                          "Print",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: null,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0))),
                  ),
                  Spacer(),
                  ButtonTheme(
                    minWidth: 100.0,
                    height: 50.0,
                    buttonColor: Colors.red,
                    child: RaisedButton(
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: null,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0))),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget ticketDetailsWidget(String firstTitle, String firstDesc,
      String secondTitle, String secondDesc) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                firstTitle,
                style: TextStyle(
                  color: Colors.grey,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Text(
                  firstDesc,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                secondTitle,
                style: TextStyle(
                  color: Colors.grey,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Text(
                  secondDesc,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
