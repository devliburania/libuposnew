import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:libupos/app/pages/point_of_sale.dart';
import 'package:libupos/app/tabs/homepage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:libupos/app/data/pojo/pos_config.dart';
import 'package:libupos/app/utility/rounded_bordered_container.dart';
import 'package:http/http.dart' as http;
import 'package:libupos/app/data/services/odoo_api.dart';
import 'package:libupos/app/data/services/odoo_response.dart';
import 'package:libupos/app/utility/strings.dart';
import 'package:libupos/base.dart';

class PosConfig extends StatefulWidget {
  final List posData;

  PosConfig({Key key, this.posData}) : super(key: key);

  @override
  _PosConfigState createState() => new _PosConfigState(posData);
}

class _PosConfigState extends Base<PosConfig> {
  List posData;
  SharedPreferences preferences;
  List<Map<String, dynamic>> sessionData = [];
  List<PosConfigData> _posconfig;

  _PosConfigState(this.posData);

  _getSession() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        showLoading();
        odoo.searchRead(Strings.pos_config, [], ['id', 'name']).then(
                (OdooResponse res) {
              if (!res.hasError()) {
                hideLoading();

                _saveSession(res.getRecords());
              }
            });
      }
    });
  }

  _getActiveSession(int posconfig_id) async {
    isConnected().then((isInternet) {
      if (isInternet) {
        showLoading();
        odoo.searchRead('pos.session', [
          ['config_id', '=', posconfig_id],
          ['state', '=', 'opened']
        ], [
          'id',
          'name',
          'display_name',
          'cash_register_balance_start',
        ]).then((OdooResponse res) {
          if (!res.hasError()) {
            print(res.getRecords());
            hideLoading();
            _saveActiveSession(res.getRecords());
            push(HomePage());
          }
        });
      }
    });
  }

  _saveSession(value) async {
    final prefs = await SharedPreferences.getInstance();
    for (var dat in value) {
      sessionData.add({'id': dat['id'], 'name': dat['name']});
    }
    prefs.setString('db_posSession', json.encode(value));
  }

  _readSession() async {
    final prefs = await SharedPreferences.getInstance();
    print('bbb');
    print(prefs.getString('db_posSession'));
    if (prefs.getString('db_posSession') != null) {
      return json.decode(prefs.getString('db_posSession'));
    } else {
      return "";
    }
    // return prefs.getString('db_posSession')
    //     ? json.decode(prefs.getString('db_posSession'))
    //     : "";
  }

  _saveActiveSession(value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('db_activeSession', json.encode(value));
  }

  @override
  void initState() {
    super.initState();
    _readSession().then((value) {
      print('aaa');
      print(value);
      if (value == "") {
        getOdooInstance().then((odoo) {
          _getSession();
        });
      } else {
        for (var dat in value) {
          sessionData.add({'id': dat['id'], 'name': dat['name']});
        }
      }
      setState(() => {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('POS Session'),
          backgroundColor: Color.fromRGBO(0, 193, 182, 1),
        ),
        body: Column(
          children: <Widget>[
            Expanded(child: _listSection(context)),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
              ),
            ),
          ],
        ));
  }

  Widget _listSection(context) {
    return Material(
      child: Container(
        child: sessionData.length > 0
            ? ListView.builder(
            itemCount: sessionData.length,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemBuilder: (context, i) =>
                InkWell(
                  onTap: () {
                    getOdooInstance().then((odoo) {
                      _getActiveSession(sessionData[i]['id']);
                    });
                  },
                  child: RoundedContainer(
                    margin: const EdgeInsets.all(8.0),
                    padding: const EdgeInsets.all(8.0),
                    color: Colors.black12,
                    child: ListTile(
                      title: Text(
                        sessionData[i]['name'],
                        style: TextStyle(
                          fontSize: 30,
                        ),
                      ),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ))
            : null,
      ),
    );
  }

  Widget _titleSection() {
    return Material(
      color: Color.fromRGBO(0, 193, 182, 0.2),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "TOTAL",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  "Rp. 0000,-",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}