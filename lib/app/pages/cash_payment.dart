import 'package:flutter/material.dart';
import 'package:libupos/base.dart';
import 'package:libupos/app/pages/thanks_page.dart';

class CashPayment extends StatefulWidget {
  final List posData;

  CashPayment({Key key, this.posData}) : super(key: key);

  @override
  _CashPaymentState createState() => new _CashPaymentState(posData);
}

class _CashPaymentState extends State<CashPayment> {
  List posData;

  _CashPaymentState(this.posData);

  TextEditingController _cashCtrler = new TextEditingController();
  double _subtotal = 0.0;
  double _changes = 0.0;

  _calculatePosData() {
    double pos_subtotal = 0.0;
    for (int i = 0; i < posData.length; i++) {
      double orderSubtotal = posData[i]['price'] * posData[i]['qty'];
      pos_subtotal += orderSubtotal;
    }
    setState(() {
      _subtotal = pos_subtotal;
    });
  }

  _calculateInput(String value) {
    var inputval = double.parse(value);

    setState(() {
      _changes = inputval - _subtotal;
    });
  }

  @override
  void initState() {
    super.initState();
    _calculatePosData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Libupos'),
        backgroundColor: Color.fromRGBO(0, 193, 182, 1),
      ),
      body: Column(
        children: <Widget>[
          _titleSection(),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
            ),
          ),
          _cashSection(),
        ],
      ),
    );
  }

  Widget _titleSection() {
    return Material(
      color: Color.fromRGBO(0, 193, 182, 0.2),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "TOTAL",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  "Rp. $_subtotal,-",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _cashSection() {
    final cashInput = TextField(
      autofocus: true,
      controller: _cashCtrler,
      keyboardType: TextInputType.number,
      onChanged: (String value) {
        _calculateInput(value);
      },
      decoration: InputDecoration(
        labelText: "Input Payment",
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
    );

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Expanded(
          child: Column(children: [
            Text('Cash Tendered'),
            SizedBox(height: 8.0),
            cashInput,
            SizedBox(height: 8.0),
            Text('Change'),
            SizedBox(height: 8.0),
            Text('Rp $_changes,-',
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(0, 193, 182, 1))),
            SizedBox(height: 40.0),
            SizedBox(
              width: double.infinity,
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              ThanksPage(
                                  posData: posData,
                                  paymentdata: _cashCtrler.text)));
                },
                child: const Text('Bayar', style: TextStyle(fontSize: 20)),
                textColor: Colors.white,
                color: Color.fromRGBO(0, 193, 182, 1),
                elevation: 5,
              ),
            )
          ]),
        ),
      ),
    );
  }
}
