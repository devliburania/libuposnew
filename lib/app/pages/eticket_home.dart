/**
 * Author: Sudip Thapa
 * profile: https://github.com/sudeepthapa
 *
 * Author: Damodar Lohani
 * profile: https://github.com/lohanidamodar
 */

import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';

// import 'package:flutter_ui_challenges/core/presentation/res/assets.dart';
// import 'package:flutter_ui_challenges/src/widgets/network_image.dart';

class EticketHome extends StatefulWidget {
  const EticketHome({Key key}) : super(key: key);

  @override
  _EticketHomeState createState() => _EticketHomeState();
}

class _EticketHomeState extends State<EticketHome> {
  String barcode;

  _createDialog(BuildContext context) {
    TextEditingController kodeBooking = TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Kode Booking"),
            content: TextField(controller: kodeBooking),
            actions: <Widget>[
              MaterialButton(
                onPressed: null,
                elevation: 5.0,
                child: Text("Kirim"),
              )
            ],
          );
        });
  }

  Widget cards(title, price) {
    if (price == '22') {
      return InkWell(
        onTap: () async {
          try {
            ScanResult barcode = await BarcodeScanner.scan();
            setState(() {
              this.barcode = barcode as String;
            });
          } on PlatformException catch (error) {
            if (error.code == BarcodeScanner.cameraAccessDenied) {
              setState(() {
                this.barcode = 'Izin kamera tidak diizinkan oleh si pengguna';
              });
            } else {
              setState(() {
                this.barcode = 'Error: $error';
              });
            }
          }
        },
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 6.0,
              ),
            ],
            color: Colors.white,
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.camera_alt,
                  size: 80,
                  color: Color.fromRGBO(0, 193, 182, 1),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(title,
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              ],
            ),
          ),
        ),
      );
    } else {
      return InkWell(
        onTap: () {
          _createDialog(context);
        },
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 6.0,
              ),
            ],
            color: Colors.white,
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.event_note,
                  size: 80,
                  color: Color.fromRGBO(0, 193, 182, 1),
                ),
                Text(title,
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              ],
            ),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(0, 193, 182, 1),
          title: Text('Libu POS'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.account_box),
              onPressed: () {
                // push(ProfilePage());
              },
            ),
          ],
        ),
        backgroundColor: Colors.white70.withOpacity(0.9),
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Container(
                height: 300,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30)),
                  color: Color.fromRGBO(0, 193, 182, 1),
                ),
                width: double.infinity,
              ),
              Container(
                margin: EdgeInsets.only(left: 90, bottom: 20),
                width: 299,
                height: 279,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(0, 180, 182, 1),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(160),
                        bottomLeft: Radius.circular(290),
                        bottomRight: Radius.circular(160),
                        topRight: Radius.circular(10))),
              ),
              CustomScrollView(
                slivers: <Widget>[
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.all(26.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 60,
                          ),
                          Text("LIBU POS",
                              style: TextStyle(
                                  fontSize: 32,
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white)),
                          Text("E-Ticketing",
                              style: TextStyle(
                                  fontSize: 32,
                                  fontWeight: FontWeight.w800,
                                  color: Colors.white)),
                          SizedBox(
                            height: 130,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SliverPadding(
                    padding: const EdgeInsets.all(26.0),
                    sliver: SliverGrid.count(
                      crossAxisCount: 2,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      children: <Widget>[
                        cards('Scan QR', '22'),
                        cards('Input No Booking', '90'),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
