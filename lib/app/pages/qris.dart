// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

// import 'package:flutter_ui_challenges/src/pages/animations/animation1/animation1.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:sunmi_flutter_print/sunmi_flutter_print.dart';
import 'package:sunmi_flutter_print/models.dart';

class QrisPage extends StatefulWidget {
  @override
  _QrisPage createState() => _QrisPage();
}

class _QrisPage extends State<QrisPage> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
    initBindPrinter();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await SunmiFlutterPrint.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  initBindPrinter() async {
    await SunmiFlutterPrint.bindPrinter();
  }

  _printQR() {
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.CENTER);
    SunmiFlutterPrint.printQRCode(text: "AAAAAAAAA", moduleSize: 7);
    // SunmiFlutterPrint.printText(text: "LIBUPOS");
    SunmiFlutterPrint.autoOutPaper();
  }

  @override
  void dispose() {
    SunmiFlutterPrint.unbindPrinter();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('QRIS'),
          backgroundColor: Color.fromRGBO(0, 193, 182, 1),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Image(image: AssetImage('assets/qr.png')),
              _printSection(context)
            ],
          ),
        ));
  }

  Widget _printSection(context) {
    return Material(
      color: Color.fromRGBO(0, 193, 182, 0.2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Material(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              child: InkWell(
                splashColor: Colors.white,
                onTap: () {
                  _printQR();
                },
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "PRINT QR",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color.fromRGBO(0, 193, 182, 1),
                          fontSize: 18,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Material(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              child: InkWell(
                splashColor: Colors.white,
                onTap: () => Navigator.pushNamed(context, "/checkout"),
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "PRINT QR",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color.fromRGBO(0, 193, 182, 1),
                          fontSize: 18,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Material(
              color: Color.fromRGBO(0, 193, 182, 1),
              borderRadius: BorderRadius.circular(10),
              elevation: 1.0,
              child: InkWell(
                splashColor: Color.fromRGBO(0, 193, 182, 1),
                onTap: () => Navigator.pushNamed(context, "/checkout"),
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "CEK TRANSAKSI",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
