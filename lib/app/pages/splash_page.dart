import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:libupos/app/data/services/odoo_api.dart';
import 'package:libupos/app/pages/login.dart';
import 'package:libupos/app/pages/pos_config.dart';
import 'package:libupos/app/tabs/homepage.dart';
import 'package:libupos/app/utility/constant.dart';
import 'package:libupos/base.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends Base<SplashPage> {
  Timer timer;
  bool isLoading = true;

  // final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading = false;
    });

    getOdooInstance();

    timer = new Timer(const Duration(milliseconds: 2000), () {
      setState(() {
        isLoading = true;
      });
      try {
        if (isLoggedIn()) {
          Navigator.of(context).pushAndRemoveUntil(
              new MaterialPageRoute(
                  builder: (BuildContext context) => PosConfig()),
                  (Route route) => route == null);
        } else {
          Navigator.of(context).pushAndRemoveUntil(
              new MaterialPageRoute(
                  builder: (BuildContext context) => Login()),
                  (Route route) => route == null);
        }
      } catch (e) {}
    });
  }


  @override
  void dispose() {
    //_t.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
          child: Center(
            child: Container(
                decoration: new BoxDecoration(
                    gradient: LinearGradient(colors: [
                      AppColors.color1,
                      AppColors.color1,
                    ], begin: Alignment.topLeft, end: Alignment.bottomRight),
                    borderRadius: new BorderRadius.all(
                        new Radius.circular(0.0))),
                child: new Container(
                  child: Stack(
                    children: <Widget>[


                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery
                                    .of(context)
                                    .size
                                    .height * .19),
                            width: 300,
                            height: 300,
                            child: new Image.asset('assets/libupos.png'),
                          ),
                          // Container(
                          //   padding: EdgeInsets.only(top: 10),
                          //   width: 400,
                          //   height: 400,
                          //   child:
                          //       new Image.asset('assets/images/logo/logotext.png'),
                          // ),
                          Container(
                              padding: EdgeInsets.only(
                                top: MediaQuery
                                    .of(context)
                                    .size
                                    .height * .15,
                              ),
                              // width: 120.0,3
                              // height: 120.0,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: MediaQuery
                                          .of(context)
                                          .size
                                          .height * .17,
                                    ),
                                    // width: 120.0,3
                                    // height: 120.0,
                                    child: SpinKitRing(
                                      color: AppColors.color2,
                                      size: 25.0,
                                      lineWidth: 2.0,
                                    ),
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ],
                  ),
                )),
          )),
    );
  }
}
