import 'package:flutter/material.dart';
import 'package:libupos/app/pages/qris.dart';
import 'package:libupos/app/pages/thanks_page.dart';
import 'package:libupos/app/pages/cash_payment.dart';
import 'package:libupos/app/utility/rounded_bordered_container.dart';

class Checkout extends StatefulWidget {
  final List posData;

  Checkout({Key key, this.posData}) : super(key: key);

  @override
  _CheckoutState createState() => new _CheckoutState(posData);
}

class _CheckoutState extends State<Checkout> {
  List posData;

  _CheckoutState(this.posData);

  double _total = 0.0;

  _calculatePosData() {
    double pos_subtotal = 0.0;
    for (int i = 0; i < posData.length; i++) {
      double orderSubtotal = posData[i]['price'] * posData[i]['qty'];
      pos_subtotal += orderSubtotal;
    }

    setState(() {
      _total = pos_subtotal;
    });
  }

  @override
  void initState() {
    super.initState();
    _calculatePosData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Checkout'),
          backgroundColor: Color.fromRGBO(0, 193, 182, 1),
        ),
        body: Column(
          children: <Widget>[
            _titleSection(),
            _paymentSection(context),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
              ),
            ),
          ],
        ));
  }

  Widget _titleSection() {
    return Material(
      color: Color.fromRGBO(0, 193, 182, 0.2),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "TOTAL",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  "Rp. $_total,-",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _paymentSection(context) {
    return Material(
      child: Container(
        child: Column(
          children: [
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => QrisPage()));
              },
              child: RoundedContainer(
                margin: const EdgeInsets.all(8.0),
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/qris.png'),
                  ),
                  title: Text(
                    "QRIS",
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            CashPayment(posData: posData)));
              },
              child: RoundedContainer(
                margin: const EdgeInsets.all(8.0),
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/libupos-logo.png'),
                  ),
                  title: Text(
                    "Cash",
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
              ),
            ),
            RoundedContainer(
              color: Colors.black38,
              margin: const EdgeInsets.all(8.0),
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage('assets/libupos-logo.png'),
                ),
                title: Text(
                  "Debit Card",
                  style: TextStyle(
                    fontSize: 30,
                  ),
                ),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
