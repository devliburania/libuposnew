import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:libupos/app/data/pojo/products.dart';
import 'package:libupos/app/data/services/odoo_api.dart';
import 'package:libupos/app/data/services/odoo_response.dart';
import 'package:libupos/app/pages/eticket_home.dart';
import 'package:libupos/app/pages/partner_details.dart';

// import 'package:libupos/app/pages/scan_qr.dart';
import 'package:libupos/app/utility/strings.dart';
import 'package:libupos/base.dart';
import 'package:libupos/app/pages/login.dart';

import 'profile.dart';
import 'settings.dart';
import 'cart.dart';

// Future<Product> fetchProduct() async {
//   final response =
//       await http.post('https://mnf.agrobogautama.co.id/api/getproduct',
//           headers: <String, String>{
//             'X-Api-key': 'xTYuaskl76510!#\$%Zhyuulattr6!',
//             'Content-Type': 'application/json',
//           },
//           body: '{}');
//   print(response.body);
//   if (response.statusCode == 200) {
//     // If the server did return a 200 OK response,
//     // then parse the JSON.
//     // return Product.fromJson(json.decode(response.body));
//   } else {
//     // If the server did not return a 200 OK response,
//     // then throw an exception.
//     throw Exception('Failed to load album');
//   }
// }

class PosPage extends StatefulWidget {
  @override
  _PosPageState createState() => _PosPageState();
}

class _PosPageState extends Base<PosPage> {
  // Future<Product> futureProduct;
  String barcode;
  List<Product> _product = [];

  int _countitem = 0;
  double _pricecount = 0;

  List<Map<String, dynamic>> sendProduct = [];

  _changeCountItemUi(i) {
    var itemcount = _countitem;
    var pricecount = _pricecount;
    var _sendProduct = sendProduct;
    var searchproduct = _sendProduct
        .where((cart) => cart['id'].toString() == i.id.toString())
        .toList();

    print(searchproduct);

    if (searchproduct.length == 0) {
      _sendProduct
          .add({'id': i.id, 'name': i.productName, 'price': i.price, 'qty': 1});
      pricecount = pricecount + i.price;
    } else {
      var qty = searchproduct[0]['qty'];
      searchproduct[0]['qty'] = qty + 1;
      pricecount = pricecount + searchproduct[0]['price'];
    }
    itemcount = itemcount + 1;

    setState(() {
      _countitem = itemcount;
      _pricecount = pricecount;
      sendProduct = _sendProduct;
    });
  }

  _getProduct() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        showLoading();
        odoo.searchRead(Strings.product_template, [],
            ['price', 'name', 'id', 'list_price']).then(
              (OdooResponse res) {
            if (!res.hasError()) {
              setState(() {
                hideLoading();
                for (var i in res.getRecords()) {
                  _product.add(
                    new Product(
                        id: i["id"],
                        productName: i["name"] is! bool ? i["name"] : "N/A",
                        price:
                        i["list_price"] is! bool ? i["list_price"] : "N/A"),
                  );
                }
              });
              _saveProduct(res.getRecords());
            } else {
              print(res.getError());
              showMessage("Warning", res.getErrorMessage());
            }
          },
        );
      }
    });
  }

  _saveProduct(value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('db_product', json.encode(value));
  }

  _readProduct() async {
    final prefs = await SharedPreferences.getInstance();
    print(prefs.getString('db_product'));
    if (prefs.getString('db_product') != null) {
      return json.decode(prefs.getString('db_product'));
    } else {
      return "";
    }
  }

  _checkProduct(){
    _readProduct().then((productdata){
      if(productdata != ""){
        // show data
        print('Product found in device');
        for (var i in productdata) {
          setState(() {
            _product.add(
            new Product(
              id: i["id"],
              productName: i["name"] is! bool ? i["name"] : "N/A",
              price:
              i["list_price"] is! bool ? i["list_price"] : "N/A"),
          );
          });
        }
      } else {
        // get data from Odoo
        print('NO Product data in device, Get data from Odoo');
        getOdooInstance().then((odoo) {
          _getProduct();
        });
      }
    });
  }

  _addProduct() {
    print('test');
  }

  @override
  void initState() {
    super.initState();
    // futureProduct = fetchProduct();
    // print(futureProduct);
    // getOdooInstance().then((odoo) {
    //   _getProduct();
    // });
    _checkProduct();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                child: Image(image: AssetImage('assets/libupos-logo.png')),
                decoration:
                BoxDecoration(color: Color.fromRGBO(0, 193, 182, 1)),
              ),
              ListTile(
                onTap: () {
                  push(EticketHome());
                },
                title: Text(
                  "E-ticket",
                  style: TextStyle(fontSize: 20),
                ),
              )
            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(0, 193, 182, 1),
          bottom: TabBar(
            tabs: [
              Tab(
                child: Text('POS'),
              ),
              Tab(
                child: Text('Simple'),
              ),
            ],
          ),
          title: Text('Libu POS'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.account_box),
              onPressed: () {
                push(ProfilePage());
              },
            ),
          ],
        ),
        body: TabBarView(
          children: [
            Stack(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: TextFormField(
                          decoration: const InputDecoration(hintText: 'Search'),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: IconButton(
                            icon: FaIcon(FontAwesomeIcons.qrcode),
                            onPressed: () async {
                              try {
                                ScanResult barcode =
                                await BarcodeScanner.scan();
                                setState(() {
                                  this.barcode = barcode as String;
                                });
                              } on PlatformException catch (error) {
                                if (error.code ==
                                    BarcodeScanner.cameraAccessDenied) {
                                  setState(() {
                                    this.barcode =
                                    'Izin kamera tidak diizinkan oleh si pengguna';
                                  });
                                } else {
                                  setState(() {
                                    this.barcode = 'Error: $error';
                                  });
                                }
                              }
                            }),
                      )
                    ],
                  ),
                ),
                // Container(
                //   padding: const EdgeInsets.only(top: 60.0, left: 10.0),
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: <Widget>[
                //       Flexible(
                //         child: TextField(
                //           decoration: const InputDecoration(hintText: 'Abc'),
                //         ),
                //       ),
                //       Flexible(
                //         child: TextField(
                //           decoration: const InputDecoration(hintText: 'Abc'),
                //         ),
                //       )
                //     ],
                //   ),
                // ),
                Scrollbar(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 65.0),
                    child: _product.length > 0
                        ? ListView.builder(
                        itemBuilder: (context, i) =>
                            InkWell(
                                onTap: () {
                                  _changeCountItemUi(_product[i]);
                                  print(sendProduct);
                                },
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        height: 75.0,
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: [
                                            Expanded(
                                                flex: 1,
                                                child: Container(
                                                  child: Center(
                                                      child: Text(
                                                        'FF',
                                                        style: TextStyle(
                                                          fontWeight:
                                                          FontWeight.w600,
                                                          fontSize: 22,
                                                          letterSpacing: 0.0,
                                                          color: Colors.black,
                                                        ),
                                                      )),
                                                  padding: const EdgeInsets.all(
                                                      10.0),
                                                  decoration: BoxDecoration(
                                                      color: Colors.orange,
                                                      borderRadius:
                                                      const BorderRadius
                                                          .all(
                                                        Radius.circular(16.0),
                                                      ),
                                                      border: Border.all(
                                                          color: Colors.grey)),
                                                )),
                                            Expanded(
                                                flex: 5,
                                                child: Padding(
                                                  padding:
                                                  const EdgeInsets.all(8.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment
                                                        .start,
                                                    children: [
                                                      Text(
                                                          _product[i]
                                                              .productName,
                                                          style: TextStyle(
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              fontSize: 14)),
                                                      Text("Rp." +
                                                          _product[i]
                                                              .price
                                                              .toString()),
                                                    ],
                                                  ),
                                                )),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                )))
                        : null,
                  ),
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  padding: const EdgeInsets.all(12.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 58,
                        height: 48,
                        child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white70,
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(16.0),
                                ),
                                border: Border.all(color: Colors.grey)),
                            child: RaisedButton(
                              onPressed: () {
                                _clearAllItems();
                              },
                              child: Text(
                                'Clear',
                                textAlign: TextAlign.center,
                              ),
                            )),
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () => push(CartTwoPage(posData: sendProduct)),
                          child: Container(
                            height: 48,
                            decoration: BoxDecoration(
                              // color: DesignCourseAppTheme.nearlyBlue,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(16.0),
                              ),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Color.fromRGBO(0, 193, 182, 1),
                                    offset: const Offset(1.1, 1.1),
                                    blurRadius: 10.0),
                              ],
                            ),
                            child: Center(
                              child: Text(
                                '$_countitem Rp $_pricecount',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  letterSpacing: 0.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Icon(Icons.access_time)
          ],
        ),
      ),
    );
  }

  void _clearAllItems() {
    _countitem = 0;
    _pricecount = 0;
    sendProduct.clear();
    setState(() {
      _countitem = _countitem;
      _pricecount = _pricecount;
      sendProduct = sendProduct;
    });
  }
}
