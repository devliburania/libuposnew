import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:libupos/app/utility/constant.dart';
import '../utility/rounded_bordered_container.dart';
import 'checkout.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:flutter_swipe_action_cell/flutter_swipe_action_cell.dart';

class ShopingCart extends StatefulWidget {
  final List posData;

  ShopingCart({Key key, this.posData}) : super(key: key);

  @override
  _ShopingCartState createState() => _ShopingCartState(posData);
}

class _ShopingCartState extends State<ShopingCart> {
  SwipeActionEditController controller;
  var formater = new NumberFormat("#,###", "en_US");
  List posData;

  _ShopingCartState(this.posData);

  double _subtotal = 0.0;
  double _ppn = 0.0;
  double _total = 0.0;

  _calculatePosData() {
    double pos_subtotal = 0.0;
    for (int i = 0; i < posData.length; i++) {
      double orderSubtotal = posData[i]['price'] * posData[i]['qty'];
      pos_subtotal += orderSubtotal;
    }

    setState(() {
      _subtotal = pos_subtotal;
      _total = pos_subtotal;
    });
  }

  _addQty(order) {
    var posorder = posData[order];
    posData[order]['qty'] = posorder['qty'] + 1;
    _calculatePosData();
  }

  _minQTy(order) {
    var posorder = posData[order];
    if (posData[order]['qty'] > 0) {
      posData[order]['qty'] = posorder['qty'] - 1;
      _calculatePosData();
    }
  }

  addKendaraan() {
    Map<String, dynamic> myObject = {
      'id': 54,
      'name': 'Bengbeng',
      'price': 1500.0,
      'qty': 1
    };
    setState(() {
      posData.add(myObject);
    });
  }

  @override
  void initState() {
    super.initState();
    print(this.posData);
    controller = SwipeActionEditController();
    _calculatePosData();
  }

  nomorKendaraan() async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            child: Container(
              height: 200,
              width: MediaQuery
                  .of(context)
                  .size
                  .width * 0.8,
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Column(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 60,
                        child: Card(
                            color: AppColors.color1,
                            elevation: 0.0,
                            margin: EdgeInsets.all(0.0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(8.0),
                                    topLeft: Radius.circular(8.0))),
                            clipBehavior: Clip.antiAlias,
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              decoration: new BoxDecoration(
                                  gradient: LinearGradient(
                                      colors: [
                                        AppColors.color1,
                                        AppColors.color1,
                                      ],
                                      begin: Alignment.topLeft,
                                      end: Alignment.center),
                                  borderRadius: new BorderRadius.all(
                                      new Radius.circular(0.0))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Icon(
                                    FontAwesomeIcons.ticketAlt,
                                    color: Colors.white,
                                    size: 17,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      "Nomor Kendaraan",
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.normal,
                                          color: Colors.white),
                                    ),
                                  )
                                ],
                              ),
                            )),
                      ),
                    ],
                  ),
                  new Container(
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        ListTile(
                          title: TextFormField(
                            //controller: tiketController,
                            textCapitalization: TextCapitalization.characters,
                            decoration: InputDecoration(
                              labelText: "Nomor Kendaraan",
                              labelStyle: TextStyle(
                                fontFamily: "Comfortaa",
                                color: Colors.black,
                                fontSize: 13.0,
                              ),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            maxLines: 1,
                            validator: (value) =>
                            value.isEmpty
                                ? "Kode Booking Tidak Boleh Kosong"
                                : null,
                            //onSaved: (value) => kodebooking = value,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding:
                    const EdgeInsets.only(top: 15, right: 10, left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(left: 10),
                          child: RaisedButton(
                            color: AppColors.color1,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            onPressed: () {
                              addKendaraan();
                            },
                            child: Text(
                              'Simpan',
                              style: new TextStyle(
                                  fontFamily: "Comfortaa",
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12.0,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget _item(int index) {
    int price = posData[index]['price'].toInt();

    return SwipeActionCell(
        controller: controller,
        index: index,
        key: ValueKey(posData[index]),
        performsFirstActionWithFullSwipe: true,
        leadingActions: [
          SwipeAction(
              title: "delete",
              onTap: (handler) async {
                await handler(true);
                posData.removeAt(index);
                _calculatePosData();
              }),
        ],
        child: Container(
          // color: Colors.black,
          // padding: const EdgeInsets.all(0),
          // margin: EdgeInsets.all(10),
          // height: 100,

          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Text(
                              posData[index]['name'],
                              overflow: TextOverflow.fade,
                              softWrap: true,
                              style: GoogleFonts.lato(
                                  fontStyle: FontStyle.normal,
                                  color: Colors.grey[500],
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Rp. " + formater.format(price),
                            style: GoogleFonts.lato(
                                fontStyle: FontStyle.normal,
                                color: Colors.red,
                                fontSize: 15.0,
                                fontWeight: FontWeight.w600),
                          ),
                          Spacer(),
                          Row(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  _minQTy(index);
                                },
                                splashColor: Colors.redAccent.shade200,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50)),
                                  alignment: Alignment.center,
                                  child: Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Icon(
                                      CupertinoIcons.minus_circled,
                                      color: Colors.green,
                                      size: 25,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50)),
                                alignment: Alignment.center,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    posData[index]['qty'].toString(),
                                    style: GoogleFonts.lato(
                                        fontStyle: FontStyle.normal,
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              InkWell(
                                onTap: () {
                                  _addQty(index);
                                },
                                splashColor: Colors.lightBlue,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50)),
                                  alignment: Alignment.center,
                                  child: Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Icon(
                                      CupertinoIcons.add_circled,
                                      color: Colors.green,
                                      size: 25,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      Container(
                        color: Colors.grey[300],
                        margin: EdgeInsets.only(left: 0, top: 5.0, right: 10),
                        height: 1.0,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // flexibleSpace: Container(
          //   decoration: BoxDecoration(
          //     image: DecorationImage(
          //         image: AssetImage("assets/appbar.png"), fit: BoxFit.fill),
          //   ),
          // ),
          backgroundColor: AppColors.color1,
          title: Text(
            "Cart",
            style: GoogleFonts.lato(
                fontStyle: FontStyle.normal,
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.w700),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                FontAwesomeIcons.car,
                color: Colors.white,
              ),
              onPressed: () {
                nomorKendaraan();
              },
            )
          ],
        ),
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/libuposbg2.png"),
                  fit: BoxFit.cover),
            ),
            child: Column(
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: posData.length > 0
                        ? ListView.builder(
                      itemCount: posData.length,
                      itemBuilder: (context, index) {
                        return _item(index);
                      },
                    )
                        : null,
                  ),
                ),
                // platNumber(),
                _checkoutSection(context)
              ],
            )));
  }

  Widget platNumber() {
    return Container(
      padding: const EdgeInsets.all(0),
      margin: EdgeInsets.all(10),
      //height: 100,
      child: Row(
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Flexible(
                        child: TextField(
                          decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.greenAccent, width: 1.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                              BorderSide(color: Colors.red, width: 1.0),
                            ),
                            hintText: 'Nomor Kendaraan',
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget cartItems(int index) {
    int price = posData[index]['price'].toInt();

    return Container(
      // color: Colors.black,
      // padding: const EdgeInsets.all(0),
      // margin: EdgeInsets.all(10),
      // height: 100,

      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          posData[index]['name'],
                          overflow: TextOverflow.fade,
                          softWrap: true,
                          style: GoogleFonts.lato(
                              fontStyle: FontStyle.normal,
                              color: Colors.grey[500],
                              fontSize: 14.0,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Rp. " + formater.format(price),
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.red,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600),
                      ),
                      Spacer(),
                      Row(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              _minQTy(index);
                            },
                            splashColor: Colors.redAccent.shade200,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50)),
                              alignment: Alignment.center,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: Icon(
                                  CupertinoIcons.minus_circled,
                                  color: Colors.green,
                                  size: 25,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50)),
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                posData[index]['qty'].toString(),
                                style: GoogleFonts.lato(
                                    fontStyle: FontStyle.normal,
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          InkWell(
                            onTap: () {
                              _addQty(index);
                            },
                            splashColor: Colors.lightBlue,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50)),
                              alignment: Alignment.center,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: Icon(
                                  CupertinoIcons.add_circled,
                                  color: Colors.green,
                                  size: 25,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  Container(
                    color: Colors.grey[300],
                    margin: EdgeInsets.only(left: 0, top: 5.0, right: 10),
                    height: 1.0,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _checkoutSection(context) {
    return Material(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            color: Colors.grey[300],
            margin: EdgeInsets.only(left: 10, top: 5.0, right: 10, bottom: 5.0),
            height: 1.0,
          ),
          Padding(
              padding: const EdgeInsets.only(top: 5.0, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: [
                      Text(
                        "Sub Total :",
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.grey[500],
                            fontSize: 15.0,
                            fontWeight: FontWeight.w700),
                      ),
                      Spacer(),
                      Text(
                        "Rp. " + formater.format(_subtotal),
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.red,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "PPN 10% :",
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.grey[500],
                            fontSize: 15.0,
                            fontWeight: FontWeight.w700),
                      ),
                      Spacer(),
                      Text(
                        "Rp. " + formater.format(_ppn),
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.red,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "Total :",
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.grey[500],
                            fontSize: 15.0,
                            fontWeight: FontWeight.w700),
                      ),
                      Spacer(),
                      Text(
                        "Rp. " + formater.format(_total),
                        style: GoogleFonts.lato(
                            fontStyle: FontStyle.normal,
                            color: Colors.red,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  )
                ],
              )),
          Container(
            color: Colors.grey[300],
            margin:
            EdgeInsets.only(left: 10, top: 10.0, right: 10, bottom: 0.0),
            height: 1.0,
          ),
          Container(
            height: 55,
            padding: const EdgeInsets.all(10.0),
            child: Material(
              color: AppColors.color1,
              borderRadius: BorderRadius.circular(10),
              elevation: 1.0,
              child: InkWell(
                splashColor: AppColors.color2,
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              Checkout(posData: posData)));
                },
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      "Bayar",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.lato(
                          fontStyle: FontStyle.normal,
                          color: Colors.white,
                          fontSize: 17.0,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
