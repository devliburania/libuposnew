// import 'package:cached_network_image/cached_network_image.dart';
import 'dart:convert';

import 'package:flutter/material.dart';

// import 'package:flutter_ui_challenges/src/pages/animations/animation1/animation1.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:sunmi_flutter_print/sunmi_flutter_print.dart';
import 'package:sunmi_flutter_print/models.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:libupos/app/data/pojo/user.dart';
import 'package:libupos/base.dart';
import 'package:libupos/app/pages/point_of_sale.dart';
import 'package:libupos/app/utility/constant.dart';

class ThanksPage extends StatefulWidget {
  final List posData;
  SharedPreferences preferences;
  final String paymentdata;

  ThanksPage({Key key, this.posData, this.paymentdata}) : super(key: key);

  @override
  _ThanksPage createState() => new _ThanksPage(posData, paymentdata);
}

class _ThanksPage extends Base<ThanksPage> {
  List posData;
  String paymentdata;
  User user;

  _ThanksPage(this.posData, this.paymentdata);

  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
    initBindPrinter();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await SunmiFlutterPrint.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  initBindPrinter() async {
    await SunmiFlutterPrint.bindPrinter();
  }

  _getSession() async {
    final prefs = await SharedPreferences.getInstance();
    var session_id = json.decode(prefs.getString(Constants.ACTIVE_SESSION));
    return session_id[0]['id'];
  }

  _getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    var user = json.decode(prefs.getString(Constants.USER_PREF));
    return user['result']['uid'];
  }

  _sendPosOrder() async {
    var session_id = await _getSession();
    var uid = await _getUserId();
    Map<String, dynamic> posOrderData = {
      "records": [
        {
          "amount_paid": this.paymentdata,
          "amount_total": this.paymentdata,
          "payment_method_id": 3, //payment method cash
          "session_id": session_id,
          "user_id": uid,
          "order_lines": this.posData
        }
      ]
    };

    _postDataApi(posOrderData);
  }

  _postDataApi(Map<String, dynamic> records) async {
    final http.Response response = await http.post(
      'http://3.0.146.165:8010/api/createorder',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Api-key':
        '830BC980F5C9AEF46E4BA7130087F9D842F5364654D8C401336A800F48BF9D5A'
      },
      body: jsonEncode(records),
    );

    if (response.statusCode == 200) {
      print('sukses kirim data');
    } else {
      throw Exception('Failed to Send Data.');
    }
  }

  _printQR() {
    List _posData = this.posData;
    var _name;
    double _total = 0;
    int _change;
    int _dibayar;
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.CENTER);
    // SunmiFlutterPrint.printQRCode(text: "AAAAAAAAA", moduleSize: 7);
    SunmiFlutterPrint.setFontSize(fontSize: 50);
    SunmiFlutterPrint.printText(text: "LIBU POS");
    SunmiFlutterPrint.setLineWrap(line: 2);
    SunmiFlutterPrint.setFontSize(fontSize: 25);
    SunmiFlutterPrint.printText(text: "Your Company");
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.LEFT);
    SunmiFlutterPrint.setLineWrap(line: 1);
    SunmiFlutterPrint.printText(text: "Telefon : 0000-0000");
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.LEFT);
    SunmiFlutterPrint.setLineWrap(line: 1);
    SunmiFlutterPrint.printText(text: "Kasir : Kasir A");
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.LEFT);
    SunmiFlutterPrint.setLineWrap(line: 2);
    for (int i = 0; i < _posData.length; i++) {
      if (_posData[i]['name']
          .toString()
          .length <= 20) {
        _name = _posData[i]['name'].toString().padRight(20);
      } else {
        _name = _posData[i]['name'].toString().substring(0, 19);
      }
      SunmiFlutterPrint.printText(
          text: _name + "     " + _posData[i]['qty'].toString() + "x");
      SunmiFlutterPrint.setAlignment(align: TEXTALIGN.LEFT);
      SunmiFlutterPrint.setLineWrap(line: 1);
      double _price = 0;
      _price = _posData[i]['qty'] * _posData[i]['price'];
      SunmiFlutterPrint.printText(text: "Rp. " + _price.toString());
      SunmiFlutterPrint.setAlignment(align: TEXTALIGN.RIGHT);
      SunmiFlutterPrint.setLineWrap(line: 1);
      _total = _total + _price;
    }
    //total
    SunmiFlutterPrint.setFontSize(fontSize: 40);
    SunmiFlutterPrint.setLineWrap(line: 1);
    SunmiFlutterPrint.printText(text: "Total : ".padRight(10));
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.LEFT);
    SunmiFlutterPrint.setLineWrap(line: 1);
    SunmiFlutterPrint.printText(text: "Rp. " + _total.toString() + ",-");
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.RIGHT);
    //dibayar
    SunmiFlutterPrint.setFontSize(fontSize: 32);
    SunmiFlutterPrint.setLineWrap(line: 1);
    SunmiFlutterPrint.printText(text: "Dibayar : ".padRight(10));
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.LEFT);
    SunmiFlutterPrint.setLineWrap(line: 1);
    SunmiFlutterPrint.printText(text: "Rp. " + this.paymentdata + ",-");
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.RIGHT);
    // Kembali
    SunmiFlutterPrint.setFontSize(fontSize: 32);
    SunmiFlutterPrint.setLineWrap(line: 1);
    SunmiFlutterPrint.printText(text: "Kembali : ".padRight(10));
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.LEFT);
    SunmiFlutterPrint.setLineWrap(line: 1);
    double _kembali = double.parse(this.paymentdata) - _total;
    SunmiFlutterPrint.printText(text: "Rp. " + _kembali.toString() + ",-");
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.RIGHT);

    SunmiFlutterPrint.setLineWrap(line: 2);
    SunmiFlutterPrint.setFontSize(fontSize: 35);
    SunmiFlutterPrint.printText(text: "== Terima Kasih ==");
    SunmiFlutterPrint.setAlignment(align: TEXTALIGN.CENTER);
    SunmiFlutterPrint.autoOutPaper();
  }

  @override
  void dispose() {
    SunmiFlutterPrint.unbindPrinter();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Terima Kasih'),
          backgroundColor: Color.fromRGBO(0, 193, 182, 1),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Image(image: AssetImage('assets/undraw_confirmed_81ex.png')),
              Text(
                "Terima Kasih",
                style: TextStyle(
                    fontSize: 40, color: Color.fromRGBO(0, 193, 182, 1)),
              ),
              _printSection(context)
            ],
          ),
        ));
  }

  Widget _printSection(context) {
    return Material(
      color: Color.fromRGBO(0, 193, 182, 0.2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Material(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              child: InkWell(
                splashColor: Colors.white,
                onTap: () {
                  _printQR();
                  _sendPosOrder();
                },
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "PRINT STRUK",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color.fromRGBO(0, 193, 182, 1),
                          fontSize: 18,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Material(
              color: Color.fromRGBO(0, 193, 182, 1),
              borderRadius: BorderRadius.circular(10),
              elevation: 1.0,
              child: InkWell(
                splashColor: Color.fromRGBO(0, 193, 182, 1),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => PosPage()));
                },
                child: Container(
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "KEMBALI KE POS",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
