import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:libupos/app/data/services/odoo_api.dart';

import 'app/pages/point_of_sale.dart';
import 'app/pages/pos_config.dart';
import 'app/pages/login.dart';
import 'app/pages/splash_page.dart';
import 'app/utility/strings.dart';
import 'base.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(App());
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends Base<App> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FlutterEasyLoading(
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: Strings.app_title,
          theme: ThemeData(
            primarySwatch: Colors.indigo,
            fontFamily: "Montserrat",
          ),
          home: SplashPage(),));
  }
}
